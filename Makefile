CC = gcc
CFLAGS = -O2 -std=c99 -Wall -Wextra -I$(IDIR)

IDIR = include/

all: queue.o image.o floodfill

queue.o: $(IDIR)queue.c $(IDIR)queue.h
	$(CC) -c $(CFLAGS) $(IDIR)queue.c

image.o: $(IDIR)image.c $(IDIR)image.h
	$(CC) -c $(CFLAGS) $(IDIR)image.c

floodfill: floodfill.c queue.o image.o
	$(CC) $(CFLAGS) floodfill.c queue.o image.o -o floodfill

.PHONY clean:
clean:
	-rm -f *.o
	-rm -f floodfill
