#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "queue.h"
#include "image.h"
#define NUMBER_OF_DIRECTIONS (8)

typedef struct Position{
    unsigned int row;
    unsigned int col;
}Position;

void flood_fill(Image * img, Position from, Color fill_color)
{
    Color filled_color = *color_at(from.row, from.col, img);
    Queue q;
    initQueue(&q);

    bool * visited = malloc(img->width * img->height * sizeof(bool));
    memset(visited, false, img->width * img->height * sizeof(bool));

    Position * _from = malloc(sizeof(Position));
    _from->row = from.row;
    _from->col = from.col;


    enqueue(&q, _from);
    while(!isEmpty(&q)){
        Position c = *(Position *)dequeue(&q);
        
        color_at(c.row, c.col, img)->R = fill_color.R;
        color_at(c.row, c.col, img)->G = fill_color.G;
        color_at(c.row, c.col, img)->B = fill_color.B;

        // N, NE, E, SE, S, SV, V, NV
        int drow[NUMBER_OF_DIRECTIONS] = {-1, -1, 0, 1, 1, 0, 0};
        int dcol[NUMBER_OF_DIRECTIONS] = {0, 1, 1, 1, 0, -1, -1, -1};


        for(int d = 0; d < NUMBER_OF_DIRECTIONS; d++){
            Position * possible = malloc(sizeof(Position));
            possible->row = c.row + drow[d];
            possible->col = c.col + dcol[d];

            bool * pos = (visited + img->width*possible->row + possible->col); 

            // If in limits of image
            if(pos >= visited && pos <= (visited + img->width*img->height)){
                    // If we haven't visited this position and it's the same color we started on
                    if(*pos == false &&
                        color_equals(color_at(possible->row, possible->col, img), &filled_color, img, img)){
                        // Mark it as visited
                        *pos = true;
                        // Add it to the queue
                        enqueue(&q, possible);
                    }

                    else{
                        free(possible);
                    }
            }

        }

    }
}

void print_usage()
{
    printf("\
\x1B[1mUsage:\x1B[0m\n\
  ./floodfill src_file dst_file st_row st_col R G B\n\
  \x1B[2m./floodfill images/google.pnm google.pnm 45 90 239 43 29\x1B[0m\n"
        );
}

int main(int argc, char * argv[])
{
    if(argc < 8){
        print_usage();
        return EXIT_FAILURE;
    }
    
    Image * img;

    FILE * img_file = fopen(argv[1], "r");
    load_pnm(img_file, &img);

    Position start_at;
    start_at.row = atoi(argv[3]);
    start_at.col = atoi(argv[4]);

    Color fill_color;
    fill_color.R = atoi(argv[5]);
    fill_color.G = atoi(argv[6]);
    fill_color.B = atoi(argv[7]);
    
    flood_fill(img, start_at, fill_color);

    FILE * result_file = fopen(argv[2], "w");
    write_pnm(result_file, img);

    return 0;
}
