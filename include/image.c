#include <stdlib.h>
#include <string.h>
#include "image.h"

/* Be aware that this will fail miserably when there are comments in the file. */
int load_pnm(FILE * in, Image ** _image)
{
    // Read file header
    char buffer[2+1];
    fscanf(in, "%2s", buffer);

    // If file is not ASCII Portable pixmap
    if(strcmp(buffer, "P3") != 0){
        return -1;
    }
    
    *_image = malloc(sizeof(Image));

    // We don't want all those pesky *s
    Image * img = *_image;
    fscanf(in, "%u", &(img->width));
    fscanf(in, "%u", &(img->height));
    fscanf(in, "%u", &(img->max_color));

    // Allocate space for the image data
    img->data = malloc(img->width * img->height * sizeof(Color));

    for(unsigned int i = 0; i < img->height; i++){
        for(unsigned int j = 0; j < img->width; j++){
            Color c;
            unsigned int t;
            fscanf(in, "%"PRIu8, &t);
            c.R = (t*255)/img->max_color;
            fscanf(in, "%"PRIu8, &t);
            c.G = (t*255)/img->max_color;
            fscanf(in, "%"PRIu8, &t);
            c.B = (t*255)/img->max_color;
            *(img->data + img->width*i + j) = c;
        }
    }
    
    return 1;
}

void write_pnm(FILE * out, Image * img)
{
    fprintf(out, "P3");
    fprintf(out, "%u %u\n", img->width, img->height);
    fprintf(out, "%u\n", img->max_color);
    
    for(unsigned int i = 0; i < img->height; i++){
        for(unsigned int j = 0; j < img->width; j++){
            Color c = *(img->data + img->width*i+j);
            fprintf(out, "%"PRIu8" %"PRIu8" %"PRIu8" ", c.R, c.G, c.B);
        }
        fprintf(out, "\n");
    }
}

Color * color_at(unsigned row, unsigned int col, Image * img)
{
    return (img->data + img->width*row + col);
}

bool color_equals(Color * c1, Color * c2, Image * i1, Image * i2)
{
    return (c1->R * 255)/(i1->max_color) == (c2->R * 255)/(i2->max_color) &&
            (c1->G * 255)/(i1->max_color) == (c2->G * 255)/(i2->max_color) &&
            (c1->B * 255)/(i1->max_color) == (c2->B * 255)/(i2->max_color);
}
