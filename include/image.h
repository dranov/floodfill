#ifndef IMAGE_H
#define IMAGE_H
#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>

typedef struct Color{
    uint8_t R;
    uint8_t G;
    uint8_t B;
}Color;

typedef struct Image{
    Color * data;

    unsigned int max_color;
    unsigned int height;
    unsigned int width;
}Image;

int load_pnm(FILE * in, Image ** _image);
void write_pnm(FILE * out, Image * img);
Color * color_at(unsigned row, unsigned int col, Image * img);
bool color_equals(Color * c1, Color * c2, Image * i1, Image * i2);
#endif
